# ![ic_launcher.png](https://bitbucket.org/repo/XqRqMR/images/2122180607-ic_launcher.png) twlkr
Android app to see timeline without signing up on Twitter. Made with [Fabric](fabric.io)

####Download Alpha version: 
[![ic_launcher.png](https://play.google.com/intl/en_us/badges/images/badge_new.png)](https://play.google.com/apps/testing/in.masukang.twlkr)

###NOTE: This repository is made public for educational purpose only

####Current Features (v1.0.0-alpha)
1. Unlimited users to follow (but see [limitation](#current-limitation))
2. No sign in required (<< is this a feature? lol)


#####Screenshots:
![](https://raw.githubusercontent.com/pnteresa/twlkr/develop/screenshots%20%26%20images/scr01.png)
![](https://raw.githubusercontent.com/pnteresa/twlkr/develop/screenshots%20%26%20images/scr02.png)
![](https://raw.githubusercontent.com/pnteresa/twlkr/develop/screenshots%20%26%20images/scr03.png)

####Current Limitation:
1. Can't fetch from private profiles.
2. 1 refresh is limited to 60 tweets
3. 1 user is limited to 15 tweets

####Future Plans (by priority, beside of fixing limitation):
1. Improve tweet sort complexity from O(n log n) to O(n)
2. Edit who to follow
3. Dynamic refresh (so it's unlimited)
4. ...much more. Let's finish them first. #nowacana

Your feedback is really valuable! [fill the feedback form here](https://docs.google.com/forms/d/1LPjuXWyXKxnW7OlyMDt978Hc83uIrxU9USGaOGWRXkA/viewform?usp=send_form)
, or
[contact/support form here](https://docs.google.com/forms/d/1bzZENzfcvzZ27ivMq5Tml-J5brKCUU2GzG3csu_A4jk/viewform?usp=send_form)

[![](https://i.creativecommons.org/l/by-nc-nd/4.0/88x31.png)](http://creativecommons.org/licenses/by-nc-nd/4.0/)

This work is licensed under a [Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License](http://creativecommons.org/licenses/by-nc-nd/4.0/).


Icon: Science Icons graphic by [Freepik](http://www.freepik.com/) from [Flaticon](http://www.flaticon.com/) is licensed under [CC BY 3.0](http://creativecommons.org/licenses/by/3.0/). Made with [Logo Maker](http://logomakr.com)